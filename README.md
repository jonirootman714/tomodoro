<div align="center">

# Pomodoro

<img src="public/tomato-logo.svg" width="200px">

🔗 [Preview](https://pomodoro-79efc.web.app/)

</div>

---

Это веб приложение основанное на методе «Помодоро».

Пользователь может запланировать несколько задач на свой день и для каждой задать примерное количество «помидоров», которое необходимо, чтобы её сделать. Верхняя задача из списка — это текущая задача.

Приложение написано на React с использованием Typescript, Tailwind, Shadcnui и со стейт-менеджером Recoil. В качестве сборщика использовался Vite.

Реализованный функционал:

- Создание задачи, удаление и ее редактирование
- Возможность установить таймер
- Работать с таймером (старт, стоп, пауза, пропустить)
- Сбор и отслеживание статистики использования таймера
  - Фокус
  - Время на паузе
  - Остановки
- Возможность выбора цветовой темы и смена режима светлая/темная тема
- Настройки таймера
  - Время дела
  - Время перерыва
  - Время долгого перерыва
  - Интервал длинного перерыва

![main](DOC/Pomodoro-main.png)
![main](DOC/Pomodoro-statistic.png)

## Запуск проекта

- install dependencies

```bash
npm i
```

- start develop server http://localhost:5173/

```bash
npm run dev
```

- lint project

```bash
npm run lint
```

- production preview

```bash
npm run preview
```

- build production

```bash
npm run build
```

## For developing

### Библиотеки/Инструменты

- React
- State manager [Recoil](https://recoiljs.org/)
- UI/Component library [Shadcn/ui](https://ui.shadcn.com/)
- Chart [recharts](https://recharts.org/)
- Icons [Lucide](https://lucide.dev/)
