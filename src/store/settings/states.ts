import { atom } from "recoil";
import { TPullAction } from "../pull/states";

const root = window.document.documentElement;
getComputedStyle(root).getPropertyValue("--primary");

enum EThemeColors {
  zinc,
  green,
  red,
  blue,
  orange,
}

const themeColors = Object.keys(EThemeColors);

export const themePrimaryColor = atom<string>({
  key: "themePrimaryColor",
  default: getComputedStyle(root).getPropertyValue("--primary"),
});

export const themeCheckedState = atom({
  key: "themeCheckedState",
  default: JSON.parse(localStorage.getItem("themeCheckedState") || "false"),
  effects: [
    ({ onSet, getInfo_UNSTABLE }) => {
      root.classList.add(
        getInfo_UNSTABLE(themeCheckedState).loadable?.contents
          ? "dark"
          : "light"
      );
      onSet((newValue) => {
        localStorage.setItem("themeCheckedState", JSON.stringify(newValue));
        root.classList.remove("dark", "light");
        root.classList.add(newValue ? "dark" : "light");
      });
    },
  ],
});

export const themeColorsState = atom({
  key: "themeColorsState",
  default: [
    {
      name: "red",
      class: "bg-red-600",
    },
    {
      name: "zinc",
      class: "bg-zinc-500",
    },
    {
      name: "green",
      class: "bg-green-700",
    },
    {
      name: "blue",
      class: "bg-blue-600",
    },
    {
      name: "orange",
      class: "bg-orange-600",
    },
  ],
});

export const themeColorState = atom({
  key: "themeColorState",
  default: localStorage.getItem("themeColorState")
    ? JSON.parse(localStorage.getItem("themeColorState") || "red")
    : "red",
  effects: [
    ({ onSet, getInfo_UNSTABLE }) => {
      root.classList.add(
        "theme-" + getInfo_UNSTABLE(themeColorState).loadable?.contents
      );
      onSet((newValue) => {
        localStorage.setItem("themeColorState", JSON.stringify(newValue));
        themeColors.forEach((color) => {
          root.classList.remove(`theme-${color}`);
        });
        root.classList.add("theme-" + newValue);
      });
    },
  ],
});

export const notificationCheckedState = atom({
  key: "notificationCheckedState",
  default: JSON.parse(
    localStorage.getItem("notificationCheckedState") || "false"
  ),
});

interface ILiveCircleConfig {
  pomodoroTime: number;
  breakTime: number;
  longBreakTime: number;
  breakCount: number;
}

export const liveCircleConfigState = atom<ILiveCircleConfig>({
  key: "liveCircleConfigState",
  default: JSON.parse(
    localStorage.getItem("liveCircleConfigState") || "false"
  ) || {
    pomodoroTime: 20,
    breakTime: 10,
    longBreakTime: 15,
    breakCount: 4,
  },
  effects: [
    ({ getInfo_UNSTABLE, onSet }) => {
      localStorage.setItem(
        "liveCircleConfigState",
        JSON.stringify(
          getInfo_UNSTABLE(liveCircleConfigState).loadable?.contents
        )
      );
      onSet((newValue) => {
        localStorage.setItem("liveCircleConfigState", JSON.stringify(newValue));
      });
    },
  ],
});

interface ILiveCircle {
  breakCountUse: number;
  timeIsActive: boolean;
  lastActionType: TPullAction;
}

export const liveCircleState = atom<ILiveCircle>({
  key: "liveCircleState",
  default: JSON.parse(localStorage.getItem("liveCircleState") || "false") || {
    breakCountUse: 0,
    timeIsActive: false,
    lastActionType: "BREAK",
  },
  effects: [
    ({ getInfo_UNSTABLE, onSet }) => {
      localStorage.setItem(
        "liveCircleState",
        JSON.stringify(getInfo_UNSTABLE(liveCircleState).loadable?.contents)
      );
      onSet((newValue) => {
        localStorage.setItem("liveCircleState", JSON.stringify(newValue));
      });
    },
  ],
});
