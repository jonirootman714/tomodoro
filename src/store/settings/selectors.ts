import { selector } from "recoil";
import { themeColorsState } from "./states";
import { generateId } from "@/utils/react/generateRandomIndex";

export const themeColorsWithId = selector({
  key: "themeColorsWithId",
  get: ({ get }) => {
    return get(themeColorsState).map((item) => (generateId(item)));
  },
});
