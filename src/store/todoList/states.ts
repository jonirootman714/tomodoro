import { atom } from "recoil";

export interface ITodoItem {
  text: string;
  id: string;
  count: number;
}

export const todoListState = atom<ITodoItem[]>({
  key: "todoListState",
  default: JSON.parse(localStorage.getItem("todoListState") || '[]') || [],
  effects: [
    ({ onSet }) => {
      onSet((currentValue) => {
        localStorage.setItem("todoListState", JSON.stringify(currentValue));
      });
    },
  ],
});
