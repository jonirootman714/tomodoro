import { selector } from "recoil";
import { todoListState } from "./states";
import { liveCircleConfigState } from "../settings/states";
import { formatTime } from "@/utils/js/formatTime";

export const isTodoListEmpty = selector({
  key: "isTodoListEmpty",
  get: ({ get }) => {
    return get(todoListState).length === 0;
  },
});

export const getTodoFirstItem = selector({
  key: "getTodoFirstItem",
  get: ({ get }) => {
    return get(todoListState)[0];
  },
});

export const getTodoTime = selector<string>({
  key: "getTodoTime",
  get: ({ get }) => {
    const { pomodoroTime } = get(liveCircleConfigState);

    const sum = get(todoListState).reduce((acc, item) => {
      let itemSum = 0;

      for (let i = 0; i < item.count; i++) {
        itemSum += pomodoroTime;
      }

      return itemSum + acc;
    }, 0);
    const { hour, hourText, isHour, isMinutes, minutes, minutesText } =
      formatTime(sum);

    return `${isHour ? `${hour} ${hourText}` : ""}${
      isHour && isMinutes ? "," : ""
    } ${isMinutes ? `${minutes} ${minutesText}` : ""}`;
  },
});
