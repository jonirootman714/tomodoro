import { selector } from "recoil";
import { ITodoItem } from "../todoList/states";
import { getTodoFirstItem, isTodoListEmpty } from "../todoList/selectors";
import { IPullItem } from "./states";
import { liveCircleConfigState, liveCircleState } from "../settings/states";

const emptyPullItem: IPullItem = {
  type: "EMPTY",
  data: { time: 0, name: "Список задач пуст" },
};

export const getPullState = selector({
  key: "getPullState",
  get: ({ get }): IPullItem[] => {
    const pullResult: IPullItem[] = [];

    if (get(isTodoListEmpty)) {
      pullResult.push(emptyPullItem);
      return pullResult;
    }

    const { pomodoroTime, breakTime, longBreakTime, breakCount } = get(
      liveCircleConfigState
    );

    const { breakCountUse, lastActionType } = get(liveCircleState);

    let longBreakCount = breakCount - breakCountUse;

    const addDeal = (todoItem: ITodoItem, i: number) => {
      pullResult.push({
        type: "DEAL",
        data: {
          time: pomodoroTime * 60,
          name: todoItem.text,
          pomodoroLeft: todoItem.count - i,
          id: todoItem.id,
        },
      });
    };

    const addBreak = () => {
      if (longBreakCount > 1) {
        pullResult.push({
          type: "BREAK",
          data: {
            time: breakTime * 60,
            name: "Легкий перерыв",
          },
        });
        longBreakCount--;
      } else {
        pullResult.push({
          type: "LONG_BREAK",
          data: {
            time: longBreakTime * 60,
            name: "Долгий перерыв",
          },
        });
        longBreakCount = breakCount;
      }
    };

    const todoItem = get(getTodoFirstItem);

    for (let i = 0; i < todoItem.count; i++) {
      if (lastActionType === "BREAK" || lastActionType === "LONG_BREAK") {
        addDeal(todoItem, i);
        addBreak();
      }

      if (lastActionType === "DEAL") {
        addBreak();
        addDeal(todoItem, i);
      }
    }

    return pullResult;
  },
});

export const getPullFirstItem = selector({
  key: "getPullFirstItem",
  get: ({ get }): IPullItem => {
    return get(getPullState)?.at(0) || emptyPullItem;
  },
});
