import { atom } from "recoil";

export type TPullAction = "DEAL" | "BREAK" | "LONG_BREAK" | "EMPTY";

export interface IPullItem {
  type: TPullAction;
  data: {
    time: number;
    name?: string;
    pomodoroLeft?: number;
    id?: string;
  };
}

export const pullState = atom<IPullItem[]>({
  key: "pullState",
  default: JSON.parse(localStorage.getItem("pullState") || "false") || [],
  effects: [
    ({ onSet }) => {
      onSet((currentValue) => {
        localStorage.setItem("pullState", JSON.stringify(currentValue));
      });
    },
  ],
});
