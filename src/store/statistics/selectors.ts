import { selector } from "recoil";
import { statisticDataState } from "./atoms";

export const statisticCurrentWeek = selector({
  key: "statisticCurrentWeek",
  get: ({ get }) => {
    return get(statisticDataState).at(-1) || [];
  },
});

export const statisticCurrentDay = selector({
  key: "statisticCurrentDay",
  get: ({ get }) => {
    const currentDayIndex = new Date().getDay();
    const currentDay = currentDayIndex === 0 ? 6 : currentDayIndex - 1;
    const currentDayData = get(statisticCurrentWeek)[currentDay];
    return { currentDayData, currentDayIndex: currentDay };
  },
});
