import { generateId } from "@/utils/react/generateRandomIndex";
import { atom } from "recoil";

export const statisticMountState = atom({
  key: "statisticMountState",
  default: false,
});

export interface IStatisticDataItem {
  name: string;
  fullName: string;
  total: number;
  stopsCount: number;
  timeOnPause: number;
  pomodoroCount: number;
  id: string;
  date: string;
}

const createStatisticDataItem = (
  dayId: number,
  isRandom: boolean = false
): IStatisticDataItem => {
  const name = Intl.DateTimeFormat("ru", { weekday: "short" }).format(
    new Date(2023, 0, dayId + 1)
  );

  const fullName = Intl.DateTimeFormat("ru", { weekday: "long" }).format(
    new Date(2023, 0, dayId + 1)
  );

  if (isRandom) {
    const total = Math.floor(Math.random() * 10000) + 1000
    return generateId({
      name: name.charAt(0).toUpperCase() + name.slice(1),
      fullName: fullName.charAt(0).toUpperCase() + fullName.slice(1),
      total,
      stopsCount: Math.floor(Math.random() * 6) + 0,
      timeOnPause: Math.floor(Math.random() * 1000) + 1,
      pomodoroCount: Math.floor( total / 600),
      date: "",
    });
  } else {
    return generateId({
      name: name.charAt(0).toUpperCase() + name.slice(1),
      fullName: fullName.charAt(0).toUpperCase() + fullName.slice(1),
      total: 0,
      stopsCount: 0,
      timeOnPause: 0,
      pomodoroCount: 0,
      date: "",
    });
  }
};

export const createWeekStatistic = (isRandom: boolean = false) => {
  const finalData = [];

  for (let i = 1; i < 7; i++) {
    finalData.push(createStatisticDataItem(i, isRandom));
  }
  finalData.push(createStatisticDataItem(0, isRandom));

  return finalData;
};

export const statisticDataState = atom<[IStatisticDataItem[]]>({
  key: "statisticDataState",
  default: JSON.parse(
    localStorage.getItem("statisticDataState") || "false"
  ) || [
    createWeekStatistic(true),
    createWeekStatistic(true),
    createWeekStatistic(),
  ],
  effects: [
    ({ getInfo_UNSTABLE, onSet }) => {
      localStorage.setItem(
        "statisticDataState",
        JSON.stringify(getInfo_UNSTABLE(statisticDataState).loadable?.contents)
      );
      onSet((newValue) => {
        localStorage.setItem("statisticDataState", JSON.stringify(newValue));
      });
    },
  ],
});
