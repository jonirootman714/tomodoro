import { atom } from "recoil";

export interface ITimer {
  seconds: number;
  minutes: number;
  play: boolean;
}

export const timerState = atom<number>({
  key: "timerState",
  default: 0,
});

export const timerOffsetState = atom<number>({
  key: "timerOffsetState",
  default: JSON.parse(localStorage.getItem("timerOffsetState") || "0"),
  effects: [
    ({ onSet, getInfo_UNSTABLE }) => {
      localStorage.setItem(
        "timerOffsetState",
        JSON.stringify(getInfo_UNSTABLE(timerOffsetState).loadable?.contents)
      );
      onSet((newValue) => {
        localStorage.setItem("timerOffsetState", JSON.stringify(newValue));
      });
    },
  ],
});

export const timerPauseState = atom<boolean>({
  key: "timerPauseState",
  default: JSON.parse(localStorage.getItem("timerPauseState") || "true"),
  effects: [
    ({ onSet, getInfo_UNSTABLE }) => {
      localStorage.setItem(
        "timerPauseState",
        JSON.stringify(getInfo_UNSTABLE(timerPauseState).loadable?.contents)
      );
      onSet((newValue) => {
        localStorage.setItem("timerPauseState", JSON.stringify(newValue));
      });
    },
  ],
});
