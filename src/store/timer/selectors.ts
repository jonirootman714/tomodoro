import { selector } from "recoil";
import { timerState } from "./states";

export const getTimeFormat = selector({
  key: "getTimeFormat",
  get: ({ get }) => {
    const time = get(timerState);
    const minutes = Math.floor(time / 60);
    const seconds = Math.floor(time % 60);

    return [`${minutes <= 9 ? "0" + minutes : minutes}:${
      seconds <= 9 ? "0" + seconds : seconds
    }`];
  },
});
