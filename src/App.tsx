import { Route, Routes } from "react-router-dom";
import { Layout } from "./components/Layout/Layout";
import { Pomodoro } from "./components/Pomodoro";
import { Statistics } from "./components/Statistics";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route path="/" element={<Pomodoro />} />
          <Route path="/statistics" element={<Statistics />} />
        </Route>
      </Routes>
    </>
  );
}

export default App;
