import { TodoList } from "./TodoList";
import { Timer } from "./Timer";
import { Instruction } from "./Instruction/Instruction";

export const Pomodoro = () => {
  return (
    <div className="grid lg:grid-cols-[520px_minmax(auto,_1fr)] gap-6 md:grid-cols-1">
      <section>
        <Instruction />
      </section>
      <section className="row-span-2">
        <Timer />
      </section>
      <section>
        <TodoList />
      </section>
    </div>
  );
};
