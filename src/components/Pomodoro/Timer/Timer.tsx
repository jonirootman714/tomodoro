import { Button } from "@/components/ui/button";
import {
  Card,
  CardContent,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import { getTodoFirstItem, isTodoListEmpty } from "@/store/todoList/selectors";
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil";
import { liveCircleState } from "@/store/settings/states";
import { useActionTodoItem } from "@/hooks/useActionTodoItem";
import { getPullFirstItem } from "@/store/pull/selectors";
import { useTimer } from "@/hooks/useTimer";
import { useEffect, useState } from "react";
import { cn } from "@/lib/utils";
import { pipe } from "@/utils/js/compositionFunctions";
import { timerOffsetState } from "@/store/timer/states";
import { getTimeFormat } from "@/store/timer/selectors";
import { useActionStatistics } from "@/hooks/useActionStatistics";
import { Clock, Pause, Play, SkipForward } from "lucide-react";
import { statisticCurrentDay } from "@/store/statistics/selectors";

interface ITimerProps {
  size?: "big" | "small";
}

export const Timer = ({ size = "big" }: ITimerProps) => {
  const [isSkip, setIsSkip] = useState(false);
  const isEmptyList = useRecoilValue(isTodoListEmpty);
  const firstTodoItem = useRecoilValue(getTodoFirstItem);
  const [timeOffset, setTimeOffset] = useRecoilState(timerOffsetState);
  const setLiveCircleState = useSetRecoilState(liveCircleState);
  const { removeItem, decrementItem } = useActionTodoItem();

  const {
    data: { time, name, pomodoroLeft },
    type,
  } = useRecoilValue(getPullFirstItem);

  const { setDateStatisticItemKey, setDateStatisticNewWeek } =
    useActionStatistics();
  const { currentDayData } = useRecoilValue(statisticCurrentDay);

  useEffect(() => {
    if (currentDayData.date === "") {
      setDateStatisticItemKey(new Date().toISOString());
    } else {
      const dateNow = new Date();
      const currentDate = new Date(Date.parse(currentDayData.date));
      if (dateNow.getDate() === currentDate.getDate()) {
        return;
      } else {
        setDateStatisticNewWeek();
      }
    }
  }, []);

  const onComplete = () => {
    if (type === "DEAL") {
      if (firstTodoItem.count > 1) {
        decrementItem(firstTodoItem);
      } else {
        removeItem(firstTodoItem);
      }

      setLiveCircleState((prev) => {
        return { ...prev, lastActionType: type };
      });
    } else if (type === "BREAK") {
      setLiveCircleState((prev) => {
        return {
          ...prev,
          lastActionType: type,
          breakCountUse: prev.breakCountUse + 1,
        };
      });
    } else if (type === "LONG_BREAK") {
      setLiveCircleState((prev) => {
        return {
          ...prev,
          lastActionType: type,
          breakCountUse: 0,
        };
      });
    }
  };

  const [{ setTime, pause, play, isPause, isComplete, isStarted, start }] =
    useTimer();
  const [timeOutput] = useRecoilValue(getTimeFormat);

  const { incrementStatisticItemKey } = useActionStatistics();

  useEffect(() => {
    setTime(time - timeOffset);
  }, [time]);

  useEffect(() => {
    if (isComplete) {
      setTimeOffset(0);
      pause();
      onComplete();
      setTime(time);
      start(false);
      if (type === "DEAL" && !isSkip) {
        incrementStatisticItemKey("pomodoroCount");
      }
    }
  }, [isComplete]);

  if (size === "big") {
    return (
      <Card>
        <CardHeader>
          <div className="flex items-center justify-between">
            <CardTitle
              className={cn(
                pomodoroLeft === undefined && "flex-1 text-center",
                "line-clamp-1 w-auto md:w-40 leading-8"
              )}
            >
              {name}
            </CardTitle>
            {pomodoroLeft !== undefined && (
              <div className="ml-2">Осталось помидоров {pomodoroLeft}</div>
            )}
          </div>
        </CardHeader>
        <CardContent className="flex items-center justify-center py-16">
          <h2 className="scroll-m-20 text-8xl md:text-9xl tracking-tight">
            {timeOutput}
          </h2>
        </CardContent>
        <CardFooter className="flex justify-evenly pb-8">
          {type === "DEAL" && (
            <>
              <Button
                onClick={() => {
                  isPause ? pipe(start, play)(true) : pause();
                }}
                disabled={isEmptyList}
                size={"lg"}
              >
                {isPause ? "Старт" : "Пауза"}
              </Button>
              <Button
                onClick={() => {
                  setTimeOffset(0);
                  pause();
                  setIsSkip(true);
                  onComplete();
                  setIsSkip(false);
                  setTime(time);
                  start(false);
                  incrementStatisticItemKey("stopsCount");
                }}
                disabled={isEmptyList || !isStarted}
                size={"lg"}
                variant={"outline"}
              >
                Пропустить
              </Button>
            </>
          )}
          {(type === "BREAK" || type === "LONG_BREAK") && (
            <Button
              onClick={() => {
                isPause
                  ? pipe(start, play)(true)
                  : (() => {
                      setTimeOffset(0);
                      pause();
                      onComplete();
                      setTime(time);
                      start(false);
                    })();
              }}
              // disabled={isEmptyList}
              size={"lg"}
            >
              {isPause ? "Старт" : "Пропустить"}
            </Button>
          )}
        </CardFooter>
      </Card>
    );
  } else {
    return (
      <div className="p-2 mr-4 flex items-center">
        <Clock className="mr-1" />
        <div className="text-lg font-bold mr-1">{timeOutput}</div>
        {type === "DEAL" && (
          <>
            <Button
              onClick={() => {
                isPause ? pipe(start, play)(true) : pause();
              }}
              disabled={isEmptyList}
              className="p-2 h-8 hover:text-primary"
              variant={"outline"}
            >
              {isPause ? (
                <Play className="w-4" size={16} />
              ) : (
                <Pause className="w-4" size={16} />
              )}
            </Button>
            {isStarted && (
              <Button
                onClick={() => {
                  setTimeOffset(0);
                  pause();
                  setIsSkip(true);
                  onComplete();
                  setIsSkip(false);
                  setTime(time);
                  start(false);
                  incrementStatisticItemKey("stopsCount");
                }}
                disabled={isEmptyList || !isStarted}
                className="ml-1 p-2 h-8 hover:text-primary"
                variant={"outline"}
              >
                <SkipForward className="w-4" size={16} />
              </Button>
            )}
          </>
        )}

        {(type === "BREAK" || type === "LONG_BREAK") && (
          <Button
            onClick={() => {
              isPause
                ? pipe(start, play)(true)
                : (() => {
                    setTimeOffset(0);
                    pause();
                    onComplete();
                    setTime(time);
                    start(false);
                  })();
            }}
            className="p-2 h-8 hover:text-primary"
            variant={"outline"}
          >
            {isPause ? (
              <Play className="w-4" size={16} />
            ) : (
              <SkipForward className="w-4" size={16} />
            )}
          </Button>
        )}
      </div>
    );
  }
};
