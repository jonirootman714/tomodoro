import { GenerateList } from "@/utils/react/GenerateList";
import { useId } from "react";

export const Instruction = () => {
  const list = [
    {
      text: "Выберите категорию и напишите название текущей задачи",
      id: useId(),
    },

    {
      text: "Запустите таймер («помидор»)",
      id: useId(),
    },
    {
      text: "Работайте пока «помидор» не прозвонит ",
      id: useId(),
    },
    {
      text: "Сделайте короткий перерыв (3-5 минут)",
      id: useId(),
    },
    {
      text: `
            Продолжайте работать «помидор» за «помидором», пока задача не будут
            выполнена. Каждые 4 «помидора» делайте длинный перерыв (15-30
            минут).
          `,
      id: useId(),
    },
  ];

  return (
    <>
      <h3 className="scroll-m-20 text-2xl font-semibold tracking-tight">
        Ура! Теперь можно начать работать:
      </h3>
      <ul className="my-3 ml-2 md:my-6 md:ml-6 list-disc [&>li]:mt-2">
        <GenerateList list={list} item={{ As: "li" }} />
      </ul>
    </>
  );
};
