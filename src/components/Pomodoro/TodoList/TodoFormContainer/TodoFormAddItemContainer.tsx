import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { preventDefault } from "@/utils/react/preventDefault";
import { ChangeEvent, FormEvent } from "react";

interface ITodoForm {
  handlerSubmit: (event: FormEvent) => void;
  text: string;
  count: number;
  min: number;
  max: number;
  placeholder?: string;
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
  onChangeCount: (event: ChangeEvent<HTMLInputElement>) => void;
}

export const TodoFormAddItemContainer = ({
  handlerSubmit,
  text,
  count,
  onChange,
  placeholder,
  onChangeCount,
  max,
  min,
}: ITodoForm) => {
  return (
    <form className="flex flex-col" onSubmit={preventDefault(handlerSubmit)}>
      <div className="flex mb-4 w-full">
        <Input value={text} onChange={onChange} placeholder={placeholder} />
        <Input
          value={count}
          type="number"
          onChange={onChangeCount}
          className="flex-1 w-20 ml-2"
          max={max}
          min={min}
        />
      </div>

      <Button disabled={text.trim().length < 1} type="submit">
        Добавить
      </Button>
    </form>
  );
};
