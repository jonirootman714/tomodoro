import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Input } from "@/components/ui/input";
import { ITodoItem } from "@/store/todoList/states";
import { preventDefault } from "@/utils/react/preventDefault";
import { ChangeEvent } from "react";

interface ITodoForm {
  handlerSubmit: (item: ITodoItem) => void;
  text: string;
  count: number;
  min: number;
  max: number;
  placeholder?: string;
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
  onChangeCount: (event: ChangeEvent<HTMLInputElement>) => void;
  itemId: string;
  id: string;
  children: React.ReactNode;
  open: boolean;
  onOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

export const TodoFormDialogEditContainer = ({
  handlerSubmit,
  text,
  count,
  onChange,
  placeholder,
  onChangeCount,
  max,
  min,
  id,
  itemId,
  children,
  open,
  onOpen,
}: ITodoForm) => {
  return (
    <Dialog onOpenChange={onOpen} open={open}>
      <DialogTrigger asChild>{children}</DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Редактировать дело</DialogTitle>
        </DialogHeader>
        <form
          id={id}
          className="flex flex-col "
          onSubmit={preventDefault(() => {
            handlerSubmit({ text, count, id: itemId });
          })}
        >
          <div className="flex mb-4 w-full">
            <Input value={text} onChange={onChange} placeholder={placeholder} />
            <Input
              value={count}
              type="number"
              onChange={onChangeCount}
              className="flex-1 w-20 ml-2"
              max={max}
              min={min}
            />
          </div>
        </form>

        <DialogFooter>
          <Button form={id} disabled={text.trim().length < 1}>
            Изменить
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};
