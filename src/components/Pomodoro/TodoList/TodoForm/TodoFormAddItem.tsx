import { ChangeEvent, useState } from "react";
import { TodoFormAddItemContainer } from "../TodoFormContainer/TodoFormAddItemContainer";
import { useActionTodoItem } from "@/hooks/useActionTodoItem";

export const TodoFormAddItem = () => {
  const [text, setText] = useState("");
  const [count, setCount] = useState(1);
  const { addItem } = useActionTodoItem();

  const onChange = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value
    const lastWord = value.split(" ").pop() || ''
    if (lastWord.length > 10) return;
    setText(value);
  };

  const min = 1;
  const max = 15;

  const onChangeCount = (event: ChangeEvent<HTMLInputElement>) => {
    const value = Math.max(min, Math.min(max, Number(event.target.value)));
    setCount(value);
  };

  const handlerSubmit = () => {
    addItem({ text: text.trim(), count });
    setText("");
    setCount(1);
  };

  return (
    <TodoFormAddItemContainer
      placeholder="Название задачи"
      handlerSubmit={handlerSubmit}
      onChange={onChange}
      onChangeCount={onChangeCount}
      text={text}
      count={count}
      min={min}
      max={max}
    />
  );
};
