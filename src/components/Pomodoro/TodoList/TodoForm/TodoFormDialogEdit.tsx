import { ChangeEvent, useId, useState } from "react";
import { TodoFormDialogEditContainer } from "../TodoFormContainer/TodoFormDialogEditContainer";
import { ITodoItem } from "@/store/todoList/states";
import { useActionTodoItem } from "@/hooks/useActionTodoItem";

interface IFormDialogEdit {
  item: ITodoItem;
  children: React.ReactNode;
}

export const TodoFormDialogEdit = ({ item, children }: IFormDialogEdit) => {
  const { editItem } = useActionTodoItem();
  const [text, setText] = useState(item.text);
  const [count, setCount] = useState(item.count);
  const [open, onOpen] = useState(false);

  const handlerSubmit = (item: ITodoItem) => {
    editItem(item);
    onOpen(false);
  };

  const onChange = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    const lastWord = value.split(" ").pop() || "";
    if (lastWord.length > 10) return;
    setText(value);
  };

  const onChangeCount = (event: ChangeEvent<HTMLInputElement>) => {
    setCount(+event.target.value);
  };

  const id = useId();

  return (
    <TodoFormDialogEditContainer
      onOpen={onOpen}
      open={open}
      onChangeCount={onChangeCount}
      onChange={onChange}
      max={15}
      min={1}
      text={text}
      count={count}
      handlerSubmit={handlerSubmit}
      id={id}
      children={children}
      itemId={item.id}
    />
  );
};
