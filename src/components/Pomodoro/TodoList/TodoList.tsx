import { todoListState } from "@/store/todoList/states";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { Card, CardHeader } from "@/components/ui/card";
import { GenerateList } from "@/utils/react/GenerateList";
import { Separator } from "@/components/ui/separator";

import { TodoFormAddItem } from "./TodoForm/TodoFormAddItem";
import { TodoItemDropdownMenu } from "./TodoItemDropdownMenu/TodoItemDropdownMenu";
import { getTodoTime, isTodoListEmpty } from "@/store/todoList/selectors";
import { useEffect } from "react";
import { liveCircleState } from "@/store/settings/states";

export const TodoList = () => {
  const todoList = useRecoilValue(todoListState);
  const isEmptyList = useRecoilValue(isTodoListEmpty);
  const spendTime = useRecoilValue(getTodoTime);

  const setLiveCircleState = useSetRecoilState(liveCircleState);

  useEffect(() => {
    isEmptyList &&
      setLiveCircleState((prev) => ({ ...prev, lastActionType: "BREAK" }));
  }, [isEmptyList]);

  return (
    <>
      <Card className="mb-6">
        <CardHeader>
          <TodoFormAddItem />
        </CardHeader>
      </Card>

      {todoList.length !== 0 && (
        <Card className="mb-6">
          <CardHeader>
            <Separator className="mt-1.5 mb-0" />
            <GenerateList
              list={todoList.map((item) => ({
                ...item,
                text: (
                  <>
                    <div className="flex items-center justify-between py-1">
                      <div className="flex items-center ">
                        <div className="flex justify-center items-center h-8 w-8 shrink-0 overflow-hidden rounded-full border border-primary text-primary mx-2 ">
                          {item.count}
                        </div>
                        <h3 className="line-clamp-1 max-w-xs">{item.text}</h3>
                      </div>
                      <TodoItemDropdownMenu item={item} />
                    </div>
                    <Separator className="mt-1.5" />
                  </>
                ),
              }))}
              item={{
                As: "div",
                className: "m-0",
              }}
            />
          </CardHeader>
        </Card>
      )}
      {spendTime}
    </>
  );
};
