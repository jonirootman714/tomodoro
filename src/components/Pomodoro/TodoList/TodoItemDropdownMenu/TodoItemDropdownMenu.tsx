import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { Button } from "@/components/ui/button";
import {
  MinusCircle,
  MoreHorizontal,
  Pencil,
  PlusCircle,
  Trash,
} from "lucide-react";
import { useActionTodoItem } from "@/hooks/useActionTodoItem";
import { ITodoItem } from "@/store/todoList/states";
import { TodoFormDialogEdit } from "../TodoForm/TodoFormDialogEdit";
import { useRecoilValue } from "recoil";
import { liveCircleState } from "@/store/settings/states";

interface ITodoItemDropdownMenu {
  item: ITodoItem;
}

export const TodoItemDropdownMenu = ({ item }: ITodoItemDropdownMenu) => {
  const { removeItem, incrementItem, decrementItem } = useActionTodoItem();
  const { timeIsActive } = useRecoilValue(liveCircleState);

  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <Button variant={"ghost"}>
          <MoreHorizontal />
        </Button>
      </DropdownMenuTrigger>
      <DropdownMenuContent>
        <DropdownMenuItem
          disabled={item.count >= 15}
          onClick={() => incrementItem(item)}
        >
          <PlusCircle className="mr-2 h-4 w-4" />
          <span>Увеличить</span>
        </DropdownMenuItem>
        <DropdownMenuItem
          disabled={item.count <= 1}
          onClick={() => decrementItem(item)}
        >
          <MinusCircle className="mr-2 h-4 w-4" />
          <span>Уменьшить</span>
        </DropdownMenuItem>
        <TodoFormDialogEdit
          item={item}
          children={
            <DropdownMenuItem onSelect={(e) => e.preventDefault()}>
              <Pencil className="mr-2 h-4 w-4" />
              <span>Редактировать</span>
            </DropdownMenuItem>
          }
        />

        <DropdownMenuItem
          disabled={timeIsActive}
          onClick={() => removeItem(item)}
        >
          <Trash className="mr-2 h-4 w-4" />
          <span>Удалить</span>
        </DropdownMenuItem>
      </DropdownMenuContent>
    </DropdownMenu>
  );
};
