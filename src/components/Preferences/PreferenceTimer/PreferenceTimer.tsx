import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import {
  liveCircleConfigState,
  liveCircleState,
} from "@/store/settings/states";
import { ChangeEvent, useId } from "react";
import { useRecoilState, useRecoilValue } from "recoil";

interface ITimerPrefItem {
  label: string;
  id: string;
  max: number;
  min: number;
  value: number;
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
}

export const PreferenceTimer = () => {
  const [config, setConfig] = useRecoilState(liveCircleConfigState);
  const { timeIsActive } = useRecoilValue(liveCircleState);

  const timerPrefItems: ITimerPrefItem[] = [
    {
      label: "Время дела (мин)",
      id: useId(),
      max: 60,
      min: 1,
      value: config.pomodoroTime,
      onChange: (event) =>
        setConfig((prev) => ({ ...prev, pomodoroTime: +event.target.value })),
    },
    {
      label: "Время перерыва (мин)",
      id: useId(),
      max: 30,
      min: 1,
      value: config.breakTime,
      onChange: (event) =>
        setConfig((prev) => ({ ...prev, breakTime: +event.target.value })),
    },
    {
      label: "Время длинного перерыва (мин)",
      id: useId(),
      max: 50,
      min: 1,
      value: config.longBreakTime,
      onChange: (event) =>
        setConfig((prev) => ({ ...prev, longBreakTime: +event.target.value })),
    },
    {
      label: "Интервал длинного перерыва",
      id: useId(),
      max: 10,
      min: 2,
      value: config.breakCount,
      onChange: (event) =>
        setConfig((prev) => ({ ...prev, breakCount: +event.target.value })),
    },
  ];

  return (
    <div className="flex flex-col">
      {timerPrefItems.map(({ label, max, min, value, id, onChange }) => (
        <div key={id} className="flex items-center mb-2">
          <Label className="flex-1 py-3" htmlFor={id}>
            {label}
          </Label>
          <Input
            max={max}
            min={min}
            className="w-20 ml-2"
            id={id}
            onChange={onChange}
            type="number"
            defaultValue={value}
            disabled={timeIsActive}
          />
        </div>
      ))}
    </div>
  );
};
