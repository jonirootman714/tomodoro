import { Label } from "@/components/ui/label";
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group";
import { cn } from "@/lib/utils";
import { themeColorsWithId } from "@/store/settings/selectors";
import { themePrimaryColor } from "@/store/settings/states";
import { GenerateList } from "@/utils/react/GenerateList";
import { SetterOrUpdater, useRecoilValue, useSetRecoilState } from "recoil";

interface IThemeProps {
  setThemeColor: SetterOrUpdater<string>;
  themeColor: string;
}

export const ThemeColors = ({ themeColor, setThemeColor }: IThemeProps) => {
  const colors = useRecoilValue(themeColorsWithId);
  const setPrimaryColor = useSetRecoilState(themePrimaryColor);

  const onValueChange = (event: string) => {
    setThemeColor(event);
    setTimeout(() => {
      setPrimaryColor(
        getComputedStyle(document.documentElement).getPropertyValue("--primary")
      );
    }, 0);
  };

  return (
    <>
      <RadioGroup
        defaultValue={themeColor}
        className="grid grid-cols-7"
        onValueChange={onValueChange}
      >
        <GenerateList
          list={colors.map(({ id, class: classNames, name }) => ({
            id,
            text: (
              <>
                <RadioGroupItem value={name} id={id} className="peer sr-only" />
                <Label
                  htmlFor={id}
                  className={cn(
                    "flex outline-offset-2 outline-2 cursor-pointer w-6 h-6 rounded-3xl peer-data-[state=checked]:outline [&:has([data-state=checked])]:outline-primary",
                    classNames
                  )}
                />
              </>
            ),
          }))}
          item={{
            className: "flex items-center justify-center max-h-6 h-6",
          }}
        />
      </RadioGroup>
    </>
  );
};
