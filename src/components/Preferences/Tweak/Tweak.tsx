import { Label } from "@/components/ui/label";
import { Switch } from "@/components/ui/switch";
import { SetterOrUpdater } from "recoil";

export interface ITweakProps {
  title: string;
  desc: string;
  isActive: boolean;
  setActive: SetterOrUpdater<boolean>;
  iconOn?: React.ReactNode;
  iconOff?: React.ReactNode;
  disabled?: boolean;
  id: string;
}

export const Tweak = (props: ITweakProps) => {
  return (
    <div className="flex items-center justify-between space-x-4">
      <Label htmlFor={props.id} className="flex flex-col space-y-1">
        <span>{props.title}</span>
        <span className="text-xs font-normal leading-snug text-muted-foreground">
          {props.desc}
        </span>
      </Label>

      <Switch
        id={props.id}
        // @ts-ignore
        icon={props.isActive ? props.iconOn : props.iconOff}
        checked={props.isActive}
        onCheckedChange={props.setActive}
        disabled={props.disabled}
      />
    </div>
  );
};
