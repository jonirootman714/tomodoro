import {
  notificationCheckedState,
  themeCheckedState,
  themeColorState,
  themePrimaryColor,
} from "@/store/settings/states";
import { useRecoilState, useSetRecoilState } from "recoil";
import { BellOff, BellRing, Moon, Settings, Sun } from "lucide-react";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";

import { CardContent, CardHeader, CardTitle } from "@/components/ui/card";
import { PreferenceTitle } from "./PreferenceTitle";
import { ThemeColors } from "./ThemeColors";
import { PreferenceTimer } from "./PreferenceTimer";
import { ITweakProps, Tweak } from "./Tweak";
import { useEffect, useId } from "react";
import { merge } from "@/utils/js/merge";

export const Preferences = () => {
  const [themeColor, setThemeColor] = useRecoilState<string>(themeColorState);
  const setPrimaryColor = useSetRecoilState(themePrimaryColor);
  const [themeChecked, setThemeChecked] =
    useRecoilState<boolean>(themeCheckedState);
  const [notificationChecked, setNotificationChecked] = useRecoilState<boolean>(
    notificationCheckedState
  );

  useEffect(() => {
    setPrimaryColor(
      getComputedStyle(document.documentElement).getPropertyValue("--primary")
    );
  });

  const preferencesTweaks: ITweakProps[] = [
    {
      title: "Цветовой режим",
      desc: "Выберете предпочитаемый цветовой режим",
      isActive: themeChecked,
      setActive: () => {
        setThemeChecked((prev) => !prev);
        setPrimaryColor(
          getComputedStyle(document.documentElement).getPropertyValue(
            "--primary"
          )
        );
      },
      iconOn: <Moon size={16} />,
      iconOff: <Sun size={16} />,
      id: useId(),
    },
    {
      title: "Всплывающее уведомление",
      desc: "Отправка уведомлений на устройство",
      isActive: notificationChecked,
      setActive: setNotificationChecked,
      iconOn: <BellRing size={14} />,
      iconOff: <BellOff size={14} />,
      disabled: true,
      id: useId(),
    },
  ];

  return (
    <Popover>
      <PopoverTrigger>
        <Settings className="hover:text-primary transition-colors" />
      </PopoverTrigger>
      <PopoverContent className="p-0 w-80" align="end">
        <CardHeader>
          <CardTitle className="text-center">Настройки</CardTitle>
        </CardHeader>
        <CardContent className="p-6 pt-0 grid gap-3">
          {preferencesTweaks.map((tweak) => (
            <Tweak {...merge(tweak)({ key: tweak.id })} />
          ))}

          <PreferenceTitle title="Цветовая палитра" />
          <ThemeColors {...{ themeColor, setThemeColor }} />
          <PreferenceTitle title="Таймер" />
          <PreferenceTimer />
        </CardContent>
      </PopoverContent>
    </Popover>
  );
};
