import { useRecoilState } from "recoil";
import { Button } from "@/components/ui/button";
import { BarChart3 } from "lucide-react";
import { Link, useLocation } from "react-router-dom";
import tomato from "@/assets/tomato-logo.svg";
import { statisticMountState } from "@/store/statistics/atoms";
import { cn } from "@/lib/utils";
import { Preferences } from "../Preferences";
import { useEffect } from "react";
import { Timer } from "../Pomodoro/Timer";

export const Header = () => {
  const [isStatisticsMount, setIsStatisticsMount] =
    useRecoilState<boolean>(statisticMountState);
  const location = useLocation();

  useEffect(() => {
    setIsStatisticsMount(location.pathname === "/statistics");
  });

  return (
    <header className="h-20 shadow-[0px_10px_63px_0px] shadow-black/10 dark:shadow-[0px_10px_63px_0px] dark:shadow-white/10">
      <nav className="container px-4 md:px-8 h-full flex items-center justify-between">
        <Button
          className="h-16 text-base md:text-2xl pl-0"
          asChild
          variant={"link"}
        >
          <Link className="flex items-center" to={"/"}>
            <img className="mr-2 w-7 md:w-9" src={tomato} />
            pomodoro_box
          </Link>
        </Button>

        <div className="flex items-center">
          <div>{isStatisticsMount && <Timer size="small" />}</div>
          <Button
            asChild
            variant={"ghost"}
            className={cn(
              isStatisticsMount && "bg-accent text-accent-foreground"
            )}
          >
            <Link to={"/statistics"} className="mr-4">
              <BarChart3 size={16} className="mr-2" /> Статистика
            </Link>
          </Button>
          <Preferences />
        </div>
      </nav>
    </header>
  );
};
