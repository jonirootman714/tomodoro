import { Header } from "../Header";
import { Outlet } from "react-router-dom";

export const Layout = () => {
  return (
    <>
      <Header />
      <main className="container px-4 py-4 md:py-24 md:px-8">
        <Outlet />
      </main>
    </>
  );
};
