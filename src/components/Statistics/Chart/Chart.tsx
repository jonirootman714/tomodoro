import { themePrimaryColor } from "@/store/settings/states";
import { IStatisticDataItem } from "@/store/statistics/atoms";
import {
  Bar,
  BarChart,
  CartesianGrid,
  Cell,
  ResponsiveContainer,
  XAxis,
  YAxis,
} from "recharts";
import { useRecoilValue } from "recoil";
import { TStatisticCellInfo } from "..";
import { formatTime } from "@/utils/js/formatTime";

interface IChart {
  data: IStatisticDataItem[];
  activeCell: number;
  setStatisticCell: React.Dispatch<React.SetStateAction<TStatisticCellInfo>>;
  setActiveCell: React.Dispatch<React.SetStateAction<number>>;
}

export const Chart = ({
  data,
  setStatisticCell,
  activeCell,
  setActiveCell,
}: IChart) => {
  const primaryColor = useRecoilValue(themePrimaryColor);

  return (
    <ResponsiveContainer width="100%" height={470}>
      <BarChart data={data}>
        <XAxis
          dataKey="name"
          stroke="#888888"
          fontSize={24}
          tickLine={false}
          axisLine={false}
        />
        <YAxis
          stroke="#888888"
          fontSize={16}
          tickLine={false}
          axisLine={false}
          tickFormatter={(value) => {
            const { hour, isHour, isMinutes, minutes, seconds } = formatTime(
              value / 60
            );
            return `${isHour ? hour + " ч" : ""} ${
              isMinutes ? minutes + " мин" : ""
            } ${seconds !== 0 && !isHour ? seconds + " с" : ""}`;
          }}
          orientation="right"
          width={100}
        />
        <CartesianGrid stroke="#888888" vertical={false} />
        <Bar
          dataKey="total"
          style={
            {
              fill: "var(--theme-primary)",
              "--theme-primary": `hsl(${primaryColor})`,
            } as React.CSSProperties
          }
          radius={[4, 4, 0, 0]}
          onClick={(_data, index) => {
            setActiveCell(index);
            setStatisticCell(_data.payload);
          }}
        >
          {data.map((entry, index) => (
            <Cell
              key={entry.id}
              cursor="pointer"
              opacity={index === activeCell ? 1 : 0.6}
            />
          ))}
        </Bar>
      </BarChart>
    </ResponsiveContainer>
  );
};
