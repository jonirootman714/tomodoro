import { Card, CardHeader } from "@/components/ui/card";
import React from "react";

interface IStatisticCellProps {
  title: string;
  icon: React.ReactNode;
  value: string | number;
}

export const StatisticCell = ({ title, icon, value }: IStatisticCellProps) => {
  return (
    <Card className="flex justify-between p-6">
      <CardHeader className="p-0 py-2 h-full justify-between">
        <h3 className="text-2xl font-semibold leading-none tracking-tight">
          {title}
        </h3>
        <h4 className="text-6xl">
          {value}
        </h4>
      </CardHeader>
      {icon}
    </Card>
  );
};
