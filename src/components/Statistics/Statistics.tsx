import {
  Card,
  CardContent,
  CardFooter,
  CardHeader,
  CardTitle,
} from "../ui/card";
import tomato from "@/assets/tomato-logo.svg";
import { Chart } from "./Chart";
import { StatisticCell } from "./StatisticCell";
import { Ban, Clock4, Target } from "lucide-react";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuRadioGroup,
  DropdownMenuRadioItem,
  DropdownMenuTrigger,
} from "../ui/dropdown-menu";
import { useEffect, useState } from "react";
import { Button } from "../ui/button";
import { useRecoilValue } from "recoil";
import {
  IStatisticDataItem,
  statisticDataState,
} from "@/store/statistics/atoms";
import { formatTime } from "@/utils/js/formatTime";
import { statisticCurrentDay } from "@/store/statistics/selectors";
import { declOfNum } from "@/utils/js/declOfNum";

type TDropdownWeek = "Эта неделя" | "Прошлая неделя" | "2 недели назад";

export type TStatisticCellInfo = Omit<IStatisticDataItem, "name" | "id">;

const defaultStatisticCellInfo = {
  fullName: "",
  total: 0,
  pomodoroCount: 0,
  stopsCount: 0,
  timeOnPause: 0,
  date: "",
};

export const Statistics = () => {
  const [week, setWeek] = useState<TDropdownWeek>("Эта неделя");
  const data = useRecoilValue(statisticDataState);
  const [currentData, setCurrentData] = useState<IStatisticDataItem[]>(
    data[data.length - 1]
  );

  const { currentDayData, currentDayIndex } =
    useRecoilValue(statisticCurrentDay);

  const [
    { fullName, total, pomodoroCount, stopsCount, timeOnPause },
    setStatisticCell,
  ] = useState<TStatisticCellInfo>(defaultStatisticCellInfo);

  const [activeCell, setActiveCell] = useState(currentDayIndex);

  const onValueChange = (value: TDropdownWeek) => {
    setActiveCell(7);
    setWeek(value);
    setStatisticCell(defaultStatisticCellInfo);
    switch (value) {
      case "Эта неделя":
        setCurrentData(data.at(-1) || []);
        setStatisticCell(currentDayData);
        setActiveCell(currentDayIndex);
        break;
      case "Прошлая неделя":
        setCurrentData(data.at(-2) || []);
        break;
      case "2 недели назад":
        setCurrentData(data.at(-3) || []);
        break;
    }
  };

  useEffect(() => {
    setStatisticCell(currentDayData);
    setActiveCell(currentDayIndex);
  }, []);

  return (
    <>
      <div className="flex justify-between items-center mb-8">
        <h2 className="scroll-m-20 pb-2 text-3xl font-semibold tracking-tight transition-colors first:mt-0">
          Ваша активность
        </h2>
        <DropdownMenu>
          <DropdownMenuTrigger asChild>
            <Button size={"lg"} variant="secondary">
              {week}
            </Button>
          </DropdownMenuTrigger>
          <DropdownMenuContent align="end">
          {/* @ts-ignore */}
            <DropdownMenuRadioGroup value={week} onValueChange={onValueChange}>
              <DropdownMenuRadioItem value="Эта неделя">
                Эта неделя
              </DropdownMenuRadioItem>
              <DropdownMenuRadioItem value="Прошлая неделя">
                Прошлая неделя
              </DropdownMenuRadioItem>
              <DropdownMenuRadioItem value="2 недели назад">
                2 недели назад
              </DropdownMenuRadioItem>
            </DropdownMenuRadioGroup>
          </DropdownMenuContent>
        </DropdownMenu>
      </div>
      <div className="grid grid-cols-[300px_minmax(auto,_1fr)] gap-8">
        <Card className="h-64 min-h-full">
          {fullName ? (
            <>
              <CardHeader>
                <CardTitle>{fullName}</CardTitle>
              </CardHeader>
              <CardContent>
                {(() => {
                  const { hour, isHour, isMinutes, minutes } = formatTime(
                    total / 60
                  );
                  if (!isHour && !isMinutes) return "Нет данных";
                  return (
                    <>
                      Вы работали над задачами в течение
                      <b className="text-primary">
                        {` ${
                          isHour
                            ? `${hour} ${declOfNum(hour, [
                                "часа",
                                "часов",
                                "часов",
                              ])}`
                            : ""
                        } ${
                          isMinutes
                            ? `${minutes} ${declOfNum(minutes, [
                                "минуты",
                                "минут",
                                "минут",
                              ])}`
                            : ""
                        }`}
                      </b>
                    </>
                  );
                })()}
              </CardContent>
            </>
          ) : (
            <CardHeader>
              <CardTitle>Нет данных</CardTitle>
            </CardHeader>
          )}
        </Card>
        <Card className="row-span-2 p-6">
          <Chart
            {...{
              setActiveCell,
              activeCell,
              data: currentData,
              setStatisticCell,
            }}
          />
        </Card>
        <Card className="flex flex-col justify-between">
          {pomodoroCount ? (
            <>
              <CardContent className="flex items-center justify-center p-6 h-full">
                <img className="mr-2 w-10 md:w-20" src={tomato} />
                <p className="text-4xl">x {pomodoroCount}</p>
              </CardContent>
              <CardFooter className="items-center justify-center text-2xl border-t p-3">
                {`${pomodoroCount} ${declOfNum(pomodoroCount, [
                  "помидор",
                  "помидора",
                  "помидоров",
                ])}`}
              </CardFooter>
            </>
          ) : (
            <CardContent className="flex items-center justify-center p-6 h-full">
              <img className="w-10 md:w-28" src={tomato} />
            </CardContent>
          )}
        </Card>
        <div className="col-span-2 grid grid-cols-[repeat(3,1fr)] gap-8">
          <StatisticCell
            icon={<Target strokeWidth={0.5} width={130} height={130} />}
            title="Фокус"
            value={`${
              Math.floor((pomodoroCount / (total / 60)) * 100) || 0
            }%`}
          />
          <StatisticCell
            icon={<Clock4 strokeWidth={0.5} width={130} height={130} />}
            title="Время на паузе"
            value={(() => {
              const { hour, isHour, minutes } = formatTime(timeOnPause / 60);
              return `${isHour ? `${hour}ч ` : ""}${minutes}м`;
            })()}
          />
          <StatisticCell
            icon={<Ban strokeWidth={0.5} width={130} height={130} />}
            title="Остановки"
            value={stopsCount}
          />
        </div>
      </div>
    </>
  );
};
