import { ITodoItem, todoListState } from "@/store/todoList/states";
import { generateId } from "@/utils/react/generateRandomIndex";
import { useSetRecoilState } from "recoil";

type TTodoItemWithoutId = Omit<ITodoItem, "id">;

interface IReturnUseActionTodoItem {
  addItem: (item: TTodoItemWithoutId) => void;
  removeItem: (item: ITodoItem) => void;
  editItem: (item: ITodoItem) => void;
  incrementItem: (item: ITodoItem) => void;
  decrementItem: (item: ITodoItem) => void;
}

export const useActionTodoItem = (): IReturnUseActionTodoItem => {
  const setTodoList = useSetRecoilState(todoListState);

  const add = (item: TTodoItemWithoutId) => {
    setTodoList((prev) => [...prev, generateId(item)]);
  };

  const edit = (item: ITodoItem) => {
    setTodoList((prev) =>
      prev.map((currentItem) =>
        currentItem.id === item.id
          ? { ...item, text: item.text.trim() }
          : currentItem
      )
    );
  };

  const remove = (item: ITodoItem) => {
    setTodoList((prev) =>
      prev.filter((currentItem) => currentItem.id !== item.id)
    );
  };

  const increment = (item: ITodoItem) => {
    setTodoList((prev) =>
      prev.map((currentItem) =>
        currentItem.id === item.id
          ? { ...currentItem, count: currentItem.count + 1 }
          : currentItem
      )
    );
  };

  const decrement = (item: ITodoItem) => {
    setTodoList((prev) =>
      prev.map((currentItem) =>
        currentItem.id === item.id
          ? { ...currentItem, count: currentItem.count - 1 }
          : currentItem
      )
    );
  };

  return {
    editItem: edit,
    addItem: add,
    removeItem: remove,
    incrementItem: increment,
    decrementItem: decrement,
  };
};
