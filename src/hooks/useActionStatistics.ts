import {
  IStatisticDataItem,
  createWeekStatistic,
  statisticDataState,
} from "@/store/statistics/atoms";
import { statisticCurrentDay } from "@/store/statistics/selectors";
import { useRecoilValue, useSetRecoilState } from "recoil";

type TStatisticDataItemOmit = Omit<
  IStatisticDataItem,
  "name" | "fullName" | "date" | "id"
>;
type TStatisticDataItemKeys = keyof TStatisticDataItemOmit;

export const useActionStatistics = () => {
  const setStatisticData = useSetRecoilState(statisticDataState);
  const { currentDayData } = useRecoilValue(statisticCurrentDay);

  const incrementStatisticItemKey = (key: TStatisticDataItemKeys) => {
    // @ts-ignore
    setStatisticData((prev) => {
      const finalData = Array.from(prev);
      const week = finalData.at(-1) || [];
      const updatedWeek = week.map((prevItem) => {
        if (prevItem.id === currentDayData.id) {
          return { ...prevItem, [key]: prevItem[key] + 1 };
        } else {
          return prevItem;
        }
      });
      finalData.pop();
      finalData.push(updatedWeek);
      return finalData;
    });
  };

  const setDateStatisticItemKey = (date: string) => {
    // @ts-ignore
    setStatisticData((prev) => {
      const finalData = Array.from(prev);
      const week = finalData.at(-1) || [];
      const updatedWeek = week.map((prevItem) => {
        if (prevItem.id === currentDayData.id) {
          return { ...prevItem, date };
        } else {
          return prevItem;
        }
      });
      finalData.pop();
      finalData.push(updatedWeek);
      return finalData;
    });
  };

  const setDateStatisticNewWeek = () => {
    // @ts-ignore
    setStatisticData((prev) => {
      const finalData = Array.from(prev);
      finalData.push(createWeekStatistic());
      return finalData;
    });
  };

  return {
    incrementStatisticItemKey,
    setDateStatisticItemKey,
    setDateStatisticNewWeek,
  };
};
