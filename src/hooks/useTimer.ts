import { liveCircleState } from "@/store/settings/states";
import {
  timerOffsetState,
  timerPauseState,
  timerState,
} from "@/store/timer/states";
import { useEffect, useRef, useState } from "react";
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil";
import { useActionStatistics } from "./useActionStatistics";
import { getPullFirstItem } from "@/store/pull/selectors";

type TRTimer = [
  {
    pause: () => void;
    play: () => void;
    start: (value: boolean) => void;
    setTime: (time: number) => void;
    isPause: boolean;
    isComplete: boolean;
    isStarted: boolean;
  }
];

export const useTimer = (): TRTimer => {
  const setTimeLeft = useSetRecoilState(timerState);
  const [timerPause, setTimerPause] = useRecoilState(timerPauseState);
  const [isComplete, setIsComplete] = useState(false);
  const [isTimerActive, setIsTimerActive] = useRecoilState(liveCircleState);
  const setTimeOffset = useSetRecoilState(timerOffsetState);

  const { type } = useRecoilValue(getPullFirstItem);

  const { incrementStatisticItemKey } = useActionStatistics();

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const timerRef = useRef<any>(null);

  useEffect(() => {
    timerRef.current = setInterval(() => {
      if (timerPause && isTimerActive.timeIsActive) {
        incrementStatisticItemKey("timeOnPause");
      }

      if (!isComplete && !timerPause && type === "DEAL") {
        incrementStatisticItemKey("total");
      }

      setTimeLeft((prevTimeLeft) => {
        if (timerPause) {
          return prevTimeLeft;
        }

        if (prevTimeLeft > 1) {
          if (isComplete) setIsComplete(false);
          return prevTimeLeft - 1;
        } else {
          setIsComplete(true);
        }

        return prevTimeLeft;
      });

      if (!(isComplete || timerPause)) {
        setTimeOffset((prev) => prev + 1);
      }
    }, 1000);

    return () => {
      clearInterval(timerRef.current);
    };
  }, [setTimeLeft, timerPause]);

  return [
    {
      pause: () => setTimerPause(true),
      play: () => setTimerPause(false),
      start: (value) =>
        setIsTimerActive((prev) => ({ ...prev, timeIsActive: value })),
      setTime: (time: number) => setTimeLeft(time),
      isPause: timerPause,
      isComplete,
      isStarted: isTimerActive.timeIsActive,
    },
  ];
};
